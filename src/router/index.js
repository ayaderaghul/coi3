import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import Dangnhap from '@/components/Dangnhap'
import Profile from '@/components/Profile'
import Dothi from '@/components/Dothi'
import Thongtin from '@/components/Thongtin'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
	{
	    path: '/',
	    name: 'Home',
	    component: Home
	},
    {
      path: '/dangnhap',
      name: 'Dangnhap',
      component: Dangnhap
    },
	{
	    path: '/profile',
	    name: 'Profile',
	    component: Profile,
	    children: [
		{
		    path: 'dothi',
		    name: 'Dothi',
		    component: Dothi
		},
		{
		    path: 'thongtin',
		    name: 'Thongtin',
		    component: Thongtin
		}
	    ]
	}
      
  ]
})
